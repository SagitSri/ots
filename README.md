[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](http://www.gnu.org/licenses/lgpl-3.0)

## OTS

This project helps to track a person's starting,ending and lost latitude and longitude and send message to registerd mobile number.


## Features
- Fully Offline map(Mapsforge VTM Maps).
- Place of Interest(POI) offline searches.
- GraphHopper to find distance between two places.
- Time count to reach to destination. 
- If time reaches to limit it will send messsage to registered mobile number.


## Screenshots

### Map Rendering
 ![Map Rendering](docs/images/android.png)
 
### POI Searching
 ![Map Rendering](docs/images/android.png)

### GraphHopper Route
 ![Map Rendering](docs/images/android.png)

### On Routing
 ![Map Rendering](docs/images/android.png)

### Alert Message
 ![Map Rendering](docs/images/android.png)
    